# from https://stackoverflow.com/a/10323583
find . -name \*.md -type f -exec pandoc -o {}.rst {} \;

# from https://stackoverflow.com/a/1225236
for file in *.md.rst
do
  mv "$file" "${file%.md.rst}.rst"
done

# https://stackoverflow.com/a/11392505
sed -i 's/.md/.html/g' *
