Splash
======

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   welcome to the Splash documentation website
  
.. container:: index-menu

   `-` :doc:`/contents` - `Forum <https://community.splashmapper.xyz>`__ - `Source code <https://gitlab.com/splashmapper/splash>`__

.. container:: index-section

   what is Splash?
  
Splash is a free video mapping software, published under the GPLv3 licence. Splash takes care of automatically calibrating the videoprojectors (intrinsic and extrinsic parameters, blending and color), and feeding them with the input video sources. This process is based on 3D models of the projection surfaces, usually created with another tool. There are some works in progress to automate the creation of the 3D models using our library `Calimiro <https://gitlab.com/splashmapper/calimiro>`__, albeit time is missing there.

Splash can handle an unlimited number of outputs, mapped to multiple 3D models. It has been tested with up to eight outputs on two graphic cards on a single computer. Support for multiple synchronized computers is planned, and Splash is known to run on ARM hardware too. In particular it runs well on NVIDIA Jetson embedded computers, and sufficiently for testing purposes on the Raspberry Pi 5.

Splash can read videos from various sources amoung which video files (most common format and Hap variations), video input (such as video cameras, capture cards), NDI video feeds, and shmdata/sh4lt (shared memory libraries used to allow for software to communicate between each others on the same computer). An addon for Blender is included which allows for exporting draft configurations and update in real-time the meshes.

.. container:: index-section

   Splash demo
  
The following video demonstrates the different steps to using Splash, from building configuration in Blender through a dedicated plugin to the projection test with real content.

.. raw:: html

   <div style="padding:56.25% 0 0 0;position:relative;"><iframe title="Splash demo video" src="https://player.vimeo.com/video/314519230?color=ff9933&api=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

.. container:: index-section

   why Splash?
  
Splash was started at a time where no other free video mapping software capable of handling any kind of projection surface to create an immersive space was available. Other software were either limited to domes or could not handle more than one output, or are not free software. With Splash we aim at facilitating the deployment of immersive experiences of any scale, from livingroom experiences to planetariums.
  
.. container:: index-section

   table of contents
   
* how to :doc:`install <install/contents>`
* :doc:`an overview of the user interface <tutorials/gui>` to get to know Splash
* :doc:`a first tutorial <tutorials/first_steps>` to get you started
* :doc:`tutorials <tutorials/contents>` to grow your skills
* :doc:`how-to guides <howto/contents>` for step-by-step instructions to accomplish specific tasks
* :doc:`Frequently Asked Questions (FAQ) <faq/contents>` for your troubleshooting needs
  
.. container:: index-section

   get in touch

If you want to share your projects done with Splash, ask questions, and help other users, the `forum <https://community.splashmapper.xyz>`__ is good place.

You can also get in touch with us through the #splashmapper channel on `Matrix <https://matrix.to/#/#splashmapper:matrix.org>`__ or by `asking questions on the issue tracker <https://gitlab.com/splashmapper/splash/-/issues>`__ .
  
.. container:: index-section

   contribute to the project

If you wish to contribute to the project, please read the `Code of Conduct <https://gitlab.com/splashmapper/splash/-/blob/main/Code_of_conduct.md>`__ and the `Contributing <https://gitlab.com/splashmapper/splash/-/blob/main/Contributing.md>`__ guide.

For more information visit `the code repository <https://gitlab.com/splashmapper/splash>`__.
  
.. container:: index-section

   who we are

This libre software is nowadays maintained by the `Lab148 cooperative <https://lab148.xyz>`__, co-founded by the lead developer of Splash.

It was initiated by the Metalab team from the Society for Arts and Technology, in Montreal. To know more about them, visit `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__.

Get in touch with us by `asking questions on the issue tracker <https://gitlab.com/splashmapper/splash/-/issues>`__ or through the #splashmapper channel on `Matrix <https://matrix.to/#/#splashmapper:matrix.org>`__.
