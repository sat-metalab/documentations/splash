# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2021, Metalab
# This file is distributed under the same license as the Splash package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Splash\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-04 10:09-0400\n"
"PO-Revision-Date: 2023-10-06 09:40-0400\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"Generated-By: Babel 2.9.1\n"
"X-Generator: Poedit 3.3.1\n"

#: ../../source/howto/calibration.rst:2
msgid "Calibration"
msgstr "Calibrage"

#: ../../source/howto/calibration.rst:4
msgid "This section covers topics about the calibration process."
msgstr "Cette section concerne le processus de calibrage."

#: ../../source/howto/calibration.rst:7 ../../source/howto/calibration.rst:41
msgid "Geometrical calibration"
msgstr "Calibrage géométrique"

#: ../../source/howto/calibration.rst:9
msgid ""
"This is a (very) preliminary guide to projector calibration in Splash, feel "
"free to report any error or useful addition."
msgstr ""
"Ceci est un guide (très) préliminaire au calibrage de projecteur avec Splash, "
"n'hésitez pas à rapporter toute erreur ou information complémentaire utile."

#: ../../source/howto/calibration.rst:12
msgid ""
"The way calibration works in Splash is to reproduce as closely as possible the "
"parameters of the physical projectors onto a virtual camera. This includes its "
"intrinsic parameters (field of view, shifts) as well as its extrinsic "
"parameters (position and orientation). Roughly, to find these parameters we "
"will set a few point - pixel pairs (at least six of them) and then ask Splash "
"to find values which minimizes the squared sum of the differences between point "
"projections and the associated pixel position."
msgstr ""
"La manière dont le calibrage fonctionne dans Splash est de reproduire aussi "
"précisément que possible les paramètres des projecteurs physiques, sur une "
"caméra virtuelle. Cela inclut ses paramètres intrinsèques (champ de vision, "
"position du centre optique) ainsi que ses paramètres extrinsèques (position et "
"orientation). En résumé, pour trouver ces paramètres nous allons spécifier "
"quelques paires point - pixel (au moins six) et demander à Splash de trouver "
"des valeurs qui minimisent le carré de la somme des différences entre la "
"projection des points et la position du pixel associé."

#: ../../source/howto/calibration.rst:21
msgid ""
"Once the configuration file is set and each videoprojector has an output, do "
"the following on the screen which has the GUI:"
msgstr ""
"Une fois que le fichier de configuration est défini, et que chaque projecteur a "
"une sortie, faire ce qui suit sur l'écran qui affiche l'interface utilisateur :"

#: ../../source/howto/calibration.rst:24
msgid "Press Ctrl + ‘Tab’ to make the GUI appear,"
msgstr "Appuyez sur Ctrl + Tab pour faire apparaître l'interface utilisateur,"

#: ../../source/howto/calibration.rst:25
msgid "open the Cameras tabulation"
msgstr "ouvrez le panneau Cameras"

#: ../../source/howto/calibration.rst:26
msgid "select the camera to calibrate by clicking in the camera list on the left,"
msgstr ""
"sélectionnez la caméra à calibrer en cliquant dessus dans la liste de caméras "
"sur la gauche,"

#: ../../source/howto/calibration.rst:28
msgid "press Ctrl + ‘W’ to switch the view to wireframe,"
msgstr "pressez Ctrl + W pour basculer vers la vue en fil de fer,"

#: ../../source/howto/calibration.rst:29
msgid "left click on a vertex to select it,"
msgstr "cliquez avec le bouton gauche sur un vertice pour le sélectionner,"

#: ../../source/howto/calibration.rst:30
msgid ""
"Shift + left click to specify the position where this vertex should be "
"projected. You can move this projection with the arrow keys,"
msgstr ""
"Shift + cliquer avec le bouton gauche de la souris pour spécifier la position "
"où ce vertice devrait être projeter. Vous pouvez déplacer cette projection avec "
"les touches fléchées,"

#: ../../source/howto/calibration.rst:32
msgid "to delete an erroneous point, Ctrl + left click on it,"
msgstr ""
"pour supprimer un point placé par erreur, Ctrl + cliquer avec le bouton gauche "
"de la souris,"

#: ../../source/howto/calibration.rst:33
msgid ""
"continue until you have seven pairs of point - projection. You can orientate "
"the view with the mouse (right click + drag) and zoom in / out with the wheel,"
msgstr ""
"continuez jusqu'à ce que vous ayez sept paires de point - projection. Vous "
"pouvez orienter la vue avec la souris (clic droit + glisser) et zoomer avant / "
"arrière avec la molette,"

#: ../../source/howto/calibration.rst:36
msgid "press ‘C’ to ask for calibration."
msgstr "pressez 'C' pour demander le calibrage."

#: ../../source/howto/calibration.rst:43
msgid ""
"At this point, you should have a first calibration. Pressing ‘C’ multiple times "
"can help getting a better one, as is adding more pairs of points - projection. "
"You can go back to previous calibration by pressing Ctrl + ‘Z’ (still while "
"hovering the camera view)."
msgstr ""
"À cette étape, vous devriez avoir un premier calibrage. Appuyer sur 'C' "
"plusieurs fois peut aider à obtenir un meilleur calibrage, tout comme ajouter "
"d'autres paires de point - projection. Vous pouvez revenir au calibrage "
"précédent en pressant Ctrl + 'Z' (tout en survolant la vue de la caméra)."

#: ../../source/howto/calibration.rst:48
msgid ""
"Once you are happy with the result, you can go back to textured rendering by "
"pressing Ctrl + ‘T’ and save the configuration by pressing Ctrl + ‘S’. It is "
"advised to save after each calibrated camera (you never know…). Also, be aware "
"that the calibration points are saved, so you will be able to update them after "
"reloading the project. They are not dependent of the 3D model, so you can for "
"example calibrate with a simple model, then change it for a high resolution "
"model of the projection surface."
msgstr ""
"Une fois satisfait du résultat, vous pouvez revenir au mode de rendu texturé en "
"pressant Ctrl + 'T', et sauvegarder la configuration en pressant Ctrl + 'S'. Il "
"est recommandé de sauvegarder après chaque caméra calibrée (on ne sait "
"jamais ...). Aussi, soyez conscient que les points de calibrage sont "
"sauvegardés, vous serez donc en mesure de les mettre à jour après avoir "
"rechargé le projet. Ils sont indépendants du modèles 3D, vous pouvez donc par "
"exemple calibrer avec un modèle simplifié, puis changer pour un modèle haute "
"définition de la surface de projection."

#: ../../source/howto/calibration.rst:58 ../../source/howto/calibration.rst:86
msgid "Projection warping"
msgstr "Déformation de la projection"

#: ../../source/howto/calibration.rst:60
msgid ""
"For these cases where you 3D model does not exactly match the projection "
"surface (lack of proper physical measures, inflatable dome, plans do not match "
"the real surface…), you can use projection warping to correct these last few "
"spots where the projectors do not match. It is meant to be used as a last "
"resort tool, as it will by definition produce a deformed output."
msgstr ""
"Pour ces cas où votre modèle 3D ne correspond pas exactement à la surface de "
"projection (manque de mesures physiques, dôme gonflable, plans qui ne "
"correspondent pas à la surface réelle ...), vous pouvez utiliser la déformation "
"de projection pour corriger les quelques dernières zones où les projecteurs ne "
"sont pas alignés. Ceci est destiné à être utilisé en dernier recours, puisque "
"par définition cela va produire une sortie déformée."

#: ../../source/howto/calibration.rst:67
msgid ""
"Projection warping allows for projection deformation according to a set of "
"control points, from which a Bezier patch is computed. The result is a "
"continuous deformation which can help making two projections match."
msgstr ""
"La déformation de projection se fait selon un ensemble de points de contrôle, à "
"partir desquels un patch de Bézier est calculé. Le résultat est une déformation "
"continue qui peut permettre à deux projections d'être alignées."

#: ../../source/howto/calibration.rst:71
msgid ""
"The user interface to control warping is similar to the camera calibration "
"interface:"
msgstr ""
"L'interface utilisateur pour le contrôle de la déformation est similaire à "
"celle du calibrage des caméras :"

#: ../../source/howto/calibration.rst:74
msgid "Press Ctrl + ‘Tab’ to open the GUI,"
msgstr "Pressez Ctrl + 'Tab' pour ouvrier l'interface utilisateur,"

#: ../../source/howto/calibration.rst:75
msgid "open the Warps tabulation,"
msgstr "ouvrez le panneau *Warps*,"

#: ../../source/howto/calibration.rst:76
msgid ""
"select the projection you want to warp by clicking in the projection list on "
"the left,"
msgstr ""
"sélectionnez la projection que vous souhaitez déformer en cliquant dessus dans "
"la liste sur la gauche,"

#: ../../source/howto/calibration.rst:78
msgid ""
"a grid appears on the desired projection: while still in the GUI, you can move "
"the control points (the vertices of the grid),"
msgstr ""
"une grille va apparaître sur la projection désirée : toujours dans l'interface "
"utilisateur, vous pouvez déplacer les points de contrôle (les vertices de la "
"grille),"

#: ../../source/howto/calibration.rst:80
msgid "when satisfied, close the Warps tabulation to make the grid disappear."
msgstr ""
"une fois satisfait, fermez le panneau Warp pour faire disparaître la grille."

#: ../../source/howto/calibration.rst:89
msgid "Color calibration"
msgstr "Calibrage colorimétrique"

#: ../../source/howto/calibration.rst:91
msgid "**Note**: color calibration is still experimental"
msgstr "**Note** : le calibrage colorimétrique est toujours expérimental"

#: ../../source/howto/calibration.rst:93
msgid ""
"Color calibration is done by capturing (automatically) a bunch of photographs "
"of the projected surface, so as to compute a common color and luminance space "
"for all the videoprojectors. Note that Splash must have been compiled with "
"GPhoto support for color calibration to be available. Also, color calibration "
"does not need any geometric calibration to be done yet, although it would not "
"make much sense to have color calibration without geometric calibration."
msgstr ""
"Le calibrage colorimétrique est fait en capturant (automatiquement) un ensemble "
"de photographies de la surface de projection, de manière à calculer un espace "
"de couleur et de luminance commun pour tous les vidéoprojecteurs. Notez que "
"Splash doit avoir été compilé avec le support de GPhoto pour que le calibrage "
"colorimétrique soit disponible. Aussi, le calibrage colorimétrique n'a pas "
"besoin que le calibrage géométrique ait déjà été fait, bien que cela ait assez "
"peut d'intérêt de faire le calibrage colorimétrique sans celui-ci."

#: ../../source/howto/calibration.rst:101
msgid ""
"Connect a PTP-compatible camera to the computer. The list of compatible cameras "
"can be found `there <http://gphoto.org/proj/libgphoto2/support.php>`__."
msgstr ""
"Connectez un appareil photo compatible PTP à l'ordinateur. La liste des "
"appareils photo compatible est disponible `ici <http://gphoto.org/proj/"
"libgphoto2/support.php>`__."

#: ../../source/howto/calibration.rst:104
msgid ""
"Set the camera in manual mode, chose sensitivity (the lower the better "
"regarding noise) and the aperture (between 1/5.6 and 1/8 to reduce vignetting). "
"Disable the autofocus mode and set the focus on the projection surface manually."
msgstr ""
"Configurez l'appareil photo en mode manuel, choisissez sa sensibilité (le mieux "
"étant de la conserver basse pour limiter le bruit) et l'ouverture (entre 1/5.6 "
"et 1/8 pour réduire le vignettage). Désactivez l'autofocus et faites le point "
"sur la surface de projection manuellement."

#: ../../source/howto/calibration.rst:108
msgid "Open the GUI by pressing Ctrl + ‘Tab’."
msgstr "Ouvrez l'interface utilisateur en pressant Ctrl + 'Tab'."

#: ../../source/howto/calibration.rst:109
msgid "Go to the Graph tabulation, find the “colorCalibrator” object in the list."
msgstr ""
"Ouvrez le panneau Graph, trouvez l'object \"colorCalibrator\" dans la liste."

#: ../../source/howto/calibration.rst:111
msgid "Set the various options, default values are a good start:"
msgstr ""
"Configurez les quelques options, les valeurs par défaut sont un bon départ :"

#: ../../source/howto/calibration.rst:113
msgid ""
"colorSamples is the number of samples taken for each channel of each projector,"
msgstr ""
"colorSamples est le nombre d'échantillons pris pour chaque canal de chaque "
"projecteur,"

#: ../../source/howto/calibration.rst:115
msgid ""
"detectionThresholdFactor has an effect on the detection of the position of each "
"projector,"
msgstr ""
"detectionThresholdFactor a un effet sur la détection de la position de chaque "
"projecteur,"

#: ../../source/howto/calibration.rst:117
msgid ""
"equalizeMethod gives the choice between various color balance equalization "
"methods:"
msgstr ""
"equalizeMethod donne le choix entre plusieurs méthodes d'égalisation de la "
"balance de couleur :"

#: ../../source/howto/calibration.rst:120
msgid "0: select a mean color balance of all projectors base balance,"
msgstr "0 : choisit une balance de couleur moyenne pour tous les projecteurs,"

#: ../../source/howto/calibration.rst:121
msgid "1: select the color balance of the weakest projector,"
msgstr ""
"1 : choisit la balance de couleur du projecteur ayant la puissance la plus "
"faible,"

#: ../../source/howto/calibration.rst:122
msgid "2: select the color balance which would give the highest global luminance,"
msgstr ""
"2 : choisit la balance de couleur qui donnerait la plus haute luminance globale,"

#: ../../source/howto/calibration.rst:125
msgid ""
"imagePerHDR sets the number of shots to create the HDR images on which color "
"values will be measured,"
msgstr ""
"imagePerHDR définit le nombre de prises de vue pour créer les images HDR à "
"partir desquelles les valeurs des couleurs sont mesurées,"

#: ../../source/howto/calibration.rst:127
msgid "hdrStep sets the stops between two shots to create an HDR image."
msgstr ""
"hdrStep définit le nombre de stops entre deux prises de vues, pour créer une "
"image HDR."

#: ../../source/howto/calibration.rst:129
msgid ""
"Press Ctrl + ‘O’ or click on “Calibrate camera response” in the Main "
"tabulation, to calibrate the camera color response,"
msgstr ""
"Pressez Ctrl + 'O' ou cliquez sur \"Calibrate camera response\" dans le panneau "
"Main, pour calibre la réponse colorimétrique de l'appareil photo,"

#: ../../source/howto/calibration.rst:131
msgid ""
"Press Ctrl + ‘P’ or click on “Calibrate displays / projectors” to launch the "
"projector calibration."
msgstr ""
"Pressez Ctrl + 'P' ou cliquez sur \"Calibrate displays / projectors\" pour "
"lancer le calibrage des projecteurs."

#: ../../source/howto/calibration.rst:134
msgid ""
"Once done, calibration is automatically activated. It can be turned off by "
"pressing Ctrl + ‘L’ or by clicking on “Activate correction”. If the process "
"went well, the luminance and color balance of the projectors should match more "
"closely. If not, there are a few things to play with:"
msgstr ""
"Une fois terminé, le calibrage est activé automatiquement. Il peut être "
"désactivé en pressant Ctrl + 'L' ou en cliquant sur \"Activate correction\". Si "
"tout s'est bien déroulé, la luminance et la balance de couleur devraient être "
"plus proches entre tous les projecteurs. Si non, voici quelques pistes à "
"explorer pour améliorer le résultat :"

#: ../../source/howto/calibration.rst:139
msgid ""
"Projector detection could have gone wrong. Check in the logs (in the console) "
"that the detected positions make sense. If not, increase or decrease the "
"detectionThresholdFactor and retry."
msgstr ""
"La détection des projecteurs s'est mal déroulée. Vérifez dans les logs (dans la "
"console) que les positions détectées sont cohérentes. Si ce n'est pas le cas, "
"augmentez ou diminuez le parametre detectionThresholdFactor puis réessayez."

#: ../../source/howto/calibration.rst:142
msgid ""
"The dynamic range of the projectors could be too wide. If so you would notice "
"in the logs that there seem to be a maximum clamping value in the HDR "
"measurements. If so, increase the imagePerHDR value."
msgstr ""
"La gamme dynamique des projecteurs pourrait être trop large. Si c'est le cas "
"vous devriez remarquer dans les logs qu'il semble y avoir une valeur maximale "
"aux mesures des HDR. Si c'est le cas, augmentez la valeur de imagePerHDR."

#: ../../source/howto/calibration.rst:146
msgid ""
"While playing with the values, do not hesitate to lower the colorSamples to "
"reduce the calibration time. Once everything seems to run, increase it again to "
"do the final calibration."
msgstr ""
"En manipulant les valeurs, n'hésitez pas à diminuer le paramètre colorSamples "
"pour réduire le temps de calibrage. Une fois que tout semble fonctionner, "
"augmentez le à nouveau pour faire le calibrage final."

#: ../../source/howto/calibration.rst:150
msgid ""
"Also, do not forget to save the calibration once you are happy with the results!"
msgstr ""
"Aussi, n'oubliez pas de sauvegarder la configuration une fois que vous êtes "
"satisfait du résultat !"

#: ../../source/howto/calibration.rst:154
msgid "Blending"
msgstr "Mélange des projections"

#: ../../source/howto/calibration.rst:156
msgid ""
"The blending algorithm uses vertex attributes to store the blending attributes. "
"An OpenGL context version of at least 4.3 activates the vertex blending, "
"otherwise no blending is available."
msgstr ""
"L'algorithme de mélange des projections utilise des attributs des vertices pour "
"stocker les valeurs de mélange. Un contexte OpenGL d'une version minimale de "
"4.3 active le mélange des projection, autrement celui-ci n'est pas disponible."

#: ../../source/howto/calibration.rst:160
msgid ""
"To activate the blending, and as long as calibration is done correctly, one "
"only has to press Ctrl + ‘B’. Pressing Ctrl + Alt + ‘B’ will recalculate the "
"blending at each frame (meaning that it can handle moving objects!)."
msgstr ""
"Pour activer le mélange, et à partir du moment où le calibrage est fait "
"correctement, vous avez juste à pressez Ctrl + 'B'. En appuyant sur Ctrl + Alt "
"+ 'B' le mélange sera recalculé à chaque nouvelle image rendue (ce qui signifie "
"que le mélange prend en c harge les objets mobiles !)."

#: ../../source/howto/calibration.rst:165
msgid ""
"Vertex blending has some known issues, most of which can be handled by tweaking "
"the blending parameters of the cameras: blendWidth and blendPrecision. To find "
"the best values, activate blending with Ctrl + Alt + ‘B’, tune the parameters, "
"and check the result."
msgstr ""
"Le mélange de projection reposant sur les vertices a quelques limitations "
"connues, la plupart pouvant être gérées en ajustement les paramètres de mélange "
"des caméras : blendWidth et BlendPrecision. Pour trouver les meilleurs valeurs, "
"activez le mélange avec Ctrl + Alt + 'B', modifiez les parametres, et vérifiez "
"le résultat."
