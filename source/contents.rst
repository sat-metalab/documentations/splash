Splash documentation
====================

.. figure:: _static/images/splash_sato.jpg
   :alt: Splash in a 20 meters fulldome

   Splash in a 20 meters fulldome

You will find all the information necessary to help you get started with
Splash in the following links:

-  :doc:`How to install Splash <install/contents>`
-  :doc:`Information on the GUI interface <tutorials/gui>`
-  :doc:`First steps with Splash <tutorials/first_steps>`
   
.. toctree::
   :maxdepth: 1
   :caption: On this site:
   
   install/contents
   tutorials/contents
   howto/contents
   faq/contents
   contributing
   reference/contents
   community
   contact
   
   Back to main page <index>
