Preferred codecs
================

Splash has been built to handled very large video feeds, but not all
video codecs can manage high resolution files correctly. Internally,
Splash uses `FFmpeg <http://ffmpeg.org/>`__ to read the video files. The
suggested codecs, based on the resolution, are the following:

================ ===== ==== ======== ===
Max. resolution  MJPEG HapQ HapAlpha Hap
================ ===== ==== ======== ===
3072x3072 @ 30Hz OK    OK   OK       OK
4096x4096 @ 30Hz       OK   OK       OK
Anything higher             OK       OK
================ ===== ==== ======== ===

Note that all these codecs have no inter-frame compression. This does
not mean that codecs with inter-frame compression can not be read, but
the listed codecs have the best performances for very high resolutions
videos. Also, codecs with inter-frame compression are not tested and
some issues can arise with these.

To convert your existing video files to one of these codecs, it is
suggested to use `FFmpeg <https://ffmpeg.org>`__. In the following
commands, replace ${INPUT_FILE} and ${OUTPUT_FILE} with the source video
file name and the output video file name.

.. code:: bash

   ffmpeg -i ${INPUT_FILE} -c:v mjpeg -c:a copy ${OUTPUT_FILE}
   ffmpeg -i ${INPUT_FILE} -c:v hap -format hap_q -chunks 8 -c:a copy ${OUTPUT_FILE}
   ffmpeg -i ${INPUT_FILE} -c:v hap -format hap_alpha -chunks 8 -c:a copy ${OUTPUT_FILE}
   ffmpeg -i ${INPUT_FILE} -c:v hap -format hap -chunks 8 -c:a copy ${OUTPUT_FILE}
