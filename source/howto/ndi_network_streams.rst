Using with NDI network streams
==============================

`NDI <https://ndi.tv/>`__ stands for Network Device Interface, and is "a
royalty-free software standard (...) to enable video-compatible products to
communicate, deliver and receive high-definition video over a computer network
(...)" (definition from `Wikipedia
<https://en.wikipedia.org/wiki/Network_Device_Interface>`__). Despite being
royalty-free, NDI is not compatible with the GPL license of Splash but it can be
used with it nonetheless.

A separate tool has been developed to convert NDI audio/video streams to and
from shmdata, the shared memory protocol supported natively by Splash. Subtly
named `ndi2shmdata`, this tool can be installed like this:

.. code:: bash

   git clone https://gitlab.com/sat-mtl/tools/ndi2shmdata
   cd ndi2shmdata
   mkdir build && cd build
   cmake -DACCEPT_NDI_LICENSE=ON ..
   make -j$(nproc) && sudo make install

Once built and installed, `ndi2shmdata` can list the streams available over the
local network:

.. code:: bash

   ndi2shmdata -l

If a stream is accessible, it should show something like this:

.. code:: bash

   SATMETA-0096 (stream)

This is basically the computer name (in uppercase) and the stream name between
parenthesis. The stream can be read from Splash by changing the media type to
`video through NDI` and change the path to the previous string, `SATMETA-00096
(stream)` in our case.

.. figure:: ../_static/images/splash_media_ndi.jpg

   Read NDI video stream

You should see the incoming stream shortly after having validated the stream
path.
