Default values file
===================

It is possible to override the hardcoded default values for the various
objects by setting up a default values files. Splash looks for such a
file based on the ``${SPLASH_DEFAULTS}`` environment variable, which
should point to a Json file with the correct structure. It is ones
responsibility to set this environment variable, for example by
specifying it right before running Splash:

.. code:: bash

   export SPLASH_DEFAULTS="${HOME}/.config/splash"

In this example, the configuration file should be located in
``${HOME}/.config/splash``, and look like this:

.. code:: json

   {
     "image_ffmpeg" : {
       "bufferSize" : 4096
     }
   }

Here, the ``bufferSize`` parameter of the ``image_ffmpeg`` type (which
is responsible for reading videos) is set to 4096MB by default,
overriding the hardcoded value of 512MB. Note that the object type may
differ from the type shown in the GUI, please refer to the following
command to find the correct type:

.. code:: bash

   splash --info
