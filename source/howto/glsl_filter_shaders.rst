GLSL filter shaders
===================

**WORK IN PROGRESS**

Things to know: 

- uniforms are automatically added as an attribute to the Filter object
- … except when the name starts with an underscore
- default uniforms are:

  - texture samplers: `_tex0`, `_tex1`, …
  - texture size: `_tex0_size`, `_tex1_size`, …
  - time since epoch: `_time`

Here is a quick example, to be improved:

.. code:: glsl

   #version 450 core

   in vec2 tc;
   out vec4 fc;

   uniform float _time;
   uniform vec2 vector = vec2(0.0, 1.0);

   void main(void)
   {
       fc = vec4(sin(_time / 1000.f), vector.x, cos(_time / 1543.f), 1.0);
   }

