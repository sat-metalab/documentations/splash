Grabbing rendered images out of Splash
======================================

Splash makes it possible to grab the rendered images (from Cameras,
Filters, Warps, etc.) and either use it internally (in a Python script
for instance) or send it to another software through
`shmdata <https://gitlab.com/sat-mtl/tools/shmdata>`__.

There are currently three types of Sinks, which work the same way but
have different options:

-  Sink, which gives access to the rendered images internally. It can be
   used in Python using the class of the same name
-  SinkShmdata, which allows for sending a rendered image to a shmdata
   shared memory
-  SinkShmdataEncoded, which also sends the rendered image to a shmdata
   shared memory, but compresses the image beforehand either using the
   h264 or h265 video compression codec.

To use a Sink, you have to connect it to an object which derives from
Texture. A Filter is added automatically as an intermediate object, as
it allows for resizing and doing some adjustments on the image before
sending.

To illustrate how to use the Sinks, we will start from a default
configuration. You should have a configuration graph looking like this
(in the Graph tabulation):

.. figure:: ../_static/images/splash_default_graph.jpg
   :alt: Splash default graph

   Splash default graph

Let’s say that we want to send the warped output of a Camera to another
software. The object we need to connect to is the Warp named
“win1_cam1_warp”. First we create a new Sink object, for example a
SinkShmdata, and we connect it to the Warp (select the warp, the
shift+left click on the new Filter).

.. figure:: ../_static/images/splash_sink_add.jpg
   :alt: Add a Filter

   Add a Filter

.. figure:: ../_static/images/splash_sink_added.jpg
   :alt: Filter added

   Filter added

Once this is done, all you have to do is to activate the Sink by setting
the ‘opened’ attribute to 1. By then you can test that the data flows
out of Splash by using the ``sdflow`` shmdata utility:

.. code:: bash

   sdflow /tmp/splash_sink

You should see frames information printed onto the screen, as well as
the caps for the data (mentioning the image size, color space, etc.). If
so, the shared memory is ready to be used by another software supporting
shmdata!

Do not hesitate to play with the other parameters, in particular the
framerate and the bitrate (for the SinkShmdataEncoded object). Also not
that by default the size of the image sent is the size of the input
image (the Warp in our case). You can resize the image using the Filter
object.

Lastly, the internal rendering of Splash is done in a linear color
space, which means that the image output from the Sink will also be
linearly encoded. You will have to convert it to sRGB on the receiving
software for the image to be displayed correctly on a sRGB display.
