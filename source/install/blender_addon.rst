Blender addon
-------------

Once Splash is compiled, a Blender addon is packaged, ready for installation. This addon allows to:

-  create a draft scene (textured objects + cameras) and export them as a Splash configuration file,
-  send in real-time, through shmdata, meshes and textures from Blender to Splash.

To install it, download it from here: `download Blender addon <https://gitlab.com/splashmapper/splash/-/jobs/artifacts/master/download?job=package:blender_addon>`__.

Open the Blender User Preferences window, select the “Add-ons” panel, then “Install from File…”. Navigate to the download directory and select ``blender_splash_addon.tar.bz2``. Now activate the addon which should appear in the “User” categorie.. It needs a recent version of Blender in order to work (2.80 or newer).

Note also that you need to have the Numpy Python module to be installed.
