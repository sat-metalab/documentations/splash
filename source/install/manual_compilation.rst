Manual compilation
==================

You can also compile Splash by hand, especially if you are curious about
its internals or want to tinker with the code (or even, who knows,
contribute!). Note that although what follows compiles the develop
branch, it is more likely to contain bugs alongside new features /
optimizations so if you experience crash you can try with the master
branch.

Installing dependencies
~~~~~~~~~~~~~~~~~~~~~~~

The packages necessary to compile Splash are the following: 

Ubuntu 22.04 and newer
----------------------

.. code:: bash

    sudo apt install build-essential clang clang-tools doxygen flatpak-builder \
       git-core git-lfs libjsoncpp-dev zip cmake cmake-extras automake lcov \
       libtool libxcb-shm0-dev libxrandr-dev libxi-dev libglm-dev libgsl0-dev \
       libatlas3-base libgphoto2-dev libxinerama-dev libxcursor-dev \
       python3-dev portaudio19-dev yasm libgl1-mesa-dev libopencv-dev \
       software-properties-common uuid-dev libavcodec-dev libavformat-dev \
       libavutil-dev libswscale-dev libsnappy-dev libzmq3-dev libwayland-dev \
       libxkbcommon-dev ninja-build pkg-config zip

Fedora
------

If not already installed, add the RPM Fusion additional package repository (needed for some of the following dependencies). This only adds the free repository:

.. code:: bash

    sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

Then install the dependencies:

.. code:: bash

    sudo dnf install gcc g++ cmake gsl-devel atlas-devel libgphoto2-devel python3-devel \
        yasm portaudio-devel python3-numpy opencv-devel jsoncpp-devel libuuid-devel libX11-devel \
        libXrandr-devel libXinerama-devel libXcursor-devel libXi-devel mesa-libGL-devel ccache \
        libavcodec-free-devel libavformat-free-devel libavutil-free-devel libswscale-free-devel \
        libasan libubsan wayland-devel libxkbcommon-devel ninja-build

Archlinux
---------

Archlinux support is not well maintained, please signal any issue.

.. code:: bash

    pacman -Sy git cmake ninja gcc yasm pkgconfig \
      libxi libxinerama libxrandr libxcursor jsoncpp mesa glm gsl libgphoto2 python3 \
      portaudio zip zlib ffmpeg opencv qt6-base vtk hdf5 glew libxkbcommon fmt make

Windows
-------

On Windows, you need to install a development environment to be able to run Splash. Fortunately there are some very nice ones, and for Splash we use `MSYS2 <https://www.msys2.org/>`__. Install it as explained on their website, then run `MSYS2 UCRT64` from the Start menu. This will give you a terminal with the correct environment to build Splash.

To finalize with the dependencies, you need to install a few ones:

.. code:: bash

    pacman -Sy --needed zip git
    pacman -Sy --needed mingw-w64-ucrt-x86_64-{cmake,make,gcc,yasm,pkgconf,jsoncpp,glm,gsl,python3,portaudio,zlib,ffmpeg,zeromq,cppzmq,snappy,opencv,gphoto2}


Building Splash
~~~~~~~~~~~~~~~

Once everything is installed, you can go on with building Splash. To build Splash and link it against the system libraries (this is the path to take on Windows):

.. code:: bash

    git clone --recurse-submodules https://gitlab.com/splashmapper/splash
    cd splash
    mkdir -p build && cd build
    # The BUILD_GENERIC_ARCH flag allows for building an executable which can run on any
    # sufficiently modern (less than 15 years) CPU. It is usually safe to remove it but
    # people had issues in the past with some arch-specific flags
    cmake -DUSE_SYSTEM_LIBS=ON -DBUILD_GENERIC_ARCH=ON ..
    ninja

Otherwise, to build and link it against the bundled libraries (note that this will not work on Windows):

.. code:: bash

    git clone --recurse-submodules https://gitlab.com/splashmapper/splash
    cd splash
    ./make_deps.sh
    mkdir -p build && cd build
    # The BUILD_GENERIC_ARCH flag allows for building an executable which can run on any
    # sufficiently modern (less than 15 years) CPU. It is usually safe to remove it but
    # people had issues in the past with some arch-specific flags
    cmake -GNinja -DBUILD_GENERIC_ARCH=ON ..
    ninja

You can now try launching Splash:

.. code:: bash

    ./src/splash --help


Installing and/or packaging
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Linux: installing from the sources
----------------------------------

Once Splash is compiled (see previous subsection), you can install it from the build directory:

.. code:: bash

    sudo ninja install
    # And then it can be run from anywhere 
    splash --help

Windows: generating a package ready to be installed and distributed
-------------------------------------------------------------------

On Windows, you can install it like on Linux using the `install` build target. But to do things more like they are done on Windows, it is suggested to generate an installation package and then install Splash like any other software. This way it will be available from the Start menu, among other advantages.

First, you need to install the Nullsoft Scriptable Install System (or NSIS), after downloading it from `their webpage <https://nsis.sourceforge.io/Main_Page>`__. This is used by CPack to generate the package. Once installed, run from the build directory:

.. code:: bash

    ninja package

An installation file named `splash-$VERSION-win64.exe` will be generated. Double-click on it from the explorer to run it and install Splash. Once done it can be found in the Start menu, or in `C:\Program Files\splash\bin`.


Uninstalling Splash (when built from sources)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To uninstall Splash when built from sources, you need to do from the very same directory where Splash has been built:

.. code:: bash

    cd ${PATH_TO_SPLASH}/build
    sudo ninja uninstall


Advanced configuration
~~~~~~~~~~~~~~~~~~~~~~

Realtime scheduling
-------------------

If you want to have access to realtime scheduling within Splash, you
need to create a group “realtime”, add yourself to it and set some
limits:

.. code:: bash

   sudo addgroup realtime
   sudo adduser $USER realtime
   sudo cp ./data/config/realtime.conf /etc/security/limits.d/

And if you want the logs to be written to /var/log/splash.log:

.. code:: bash

   sudo adduser $USER syslog

Then log out and log back in.

Attributes default values
-------------------------

If you want to specify some defaults values for the objects, you can set
the environment variable SPLASH_DEFAULTS with the path to a file
defining default values for given types. An example of such a file can
be found in
`data/config/splashrc <https://gitlab.com/splashmapper/splash/tree/master/data/config/splashrc>`__

To uninstall Splash when installed from the sources, do the following from the directory Splash was built in:

.. code:: bash

    cd ${PATH_TO_SPLASH}/build
    sudo make uninstall

Wayland support
---------------

**Support for the Wayland display server is partial**, and follows the progress of the GLFW library which is used to give a cross-platform way to handle graphic contexts. An example of a current limitation is that if any Splash window is hidden, the whole rendering will be stalled on some Wayland compositors.

To activate it, the `SPLASH_USE_WAYLAND` environment variable must be defined, to whichever value. For example, Splash can be run like this:

```bash
SPLASH_USE_WAYLAND=1 splash
```
