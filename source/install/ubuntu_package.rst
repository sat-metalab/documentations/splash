Ubuntu package
==============

Download the package
--------------------

Here are the links to the latest Ubuntu packages for Intel/AMD CPUs:

- `Ubuntu 22.04 <https://gitlab.com/splashmapper/splash/-/jobs/artifacts/master/download?job=package:debian:22.04:amd64>`__.
- `Ubuntu 24.04 <https://gitlab.com/splashmapper/splash/-/jobs/artifacts/master/download?job=package:debian:24.04:amd64>`__.

And for ARM64 CPUs:

- `Ubuntu 22.04 <https://gitlab.com/splashmapper/splash/-/jobs/artifacts/master/download?job=package:debian:22.04:arm64>`__.
- `Ubuntu 24.04 <https://gitlab.com/splashmapper/splash/-/jobs/artifacts/master/download?job=package:debian:24.04:arm64>`__.

Install the package
-------------------

To install the package, you have to unzip the file previously downloaded and
type the following command in a terminal: 

.. code:: bash

   sudo apt install <download path>/splash_<ref_name>.deb

Double clicking on the Debian file should also trigger its installation.

Uninstall Splash
----------------

The process to uninstall Splash is the same as any other software installed on Ubuntu.

It can be done from the Software manager, or from the command line:

.. code:: bash

   sudo apt remove splash-mapper
